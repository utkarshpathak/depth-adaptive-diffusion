clear;

restoredefaultpath;
addpath('../build/lib');

%Params:
lambda = single(50);
t1 = single(1/sqrt(3));
t2 = single(1/sqrt(3));
nbiter=1500;
stepGamma = single(1);
nblabels = single(25);
batch_size = single(50);
order_3d = [2, 1, 3];
order_4d = [2, 1, 3, 4];

%I_L = im2double(imread('../images/scene1.row3.col2.ppm'));
%I_R = im2double(imread('../images/scene1.row3.col4.ppm'));
%height = size(I_L, 1);
%width = size(I_L, 2);

I_L = permute(single(im2double(imread('../images/scene1.row3.col2.ppm'))), order_3d);
I_R = permute(single(im2double(imread('../images/scene1.row3.col4.ppm'))), order_3d);
height = size(I_L, 2);
width = size(I_L, 1);

%rho = permute(single(computeRho(I_L, I_R, nblabels, 50)), order_3d);
rho = libMEX('computeRho', I_L, I_R, nblabels, lambda);

Phi = single(zeros(height, width, nblabels));
Phi(:, :, 1) = 1;
Phi = permute(Phi, order_3d);
P = single(zeros(height, width, nblabels, 3));
P = permute(P, order_4d);

labels = single(0:stepGamma:nblabels-1);

f1 = figure('Name','Phi');
any_diff = 0;

for k=1:batch_size:nbiter
    ticiter = tic;

    [Phi, P] = libMEX('iterateUpdate', Phi, P, rho, t1, t2, stepGamma, batch_size);
    
    toc(ticiter);
    cuda_image = libMEX('constructImageFromPhi', ipermute(Phi, order_3d), labels(:));
    matlab_image = ConstructImageFromPhi(ipermute(Phi, order_3d),labels);
    diff = abs(cuda_image - matlab_image);
    diff_min = min(diff(:));
    diff_max = max(diff(:));
    if diff_max > 0
        any_diff = 1;
    end
    imshow(cuda_image,[]);
end
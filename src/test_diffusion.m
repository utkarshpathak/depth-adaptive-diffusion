%%
clc
clear all;

restoredefaultpath;

addpath(genpath('../build/lib/'))

%Params:
lambda = single(50);
t1 = single(1/sqrt(3));
t2 = single(1/sqrt(3));
nbiter = single(1000);
stepGamma = single(1);
nblabels = single(25);
order_3d = [2, 1, 3];
order_4d = [2, 1, 3, 4];
measurement_iter = single(100);
bluriter = single(100);
weights = single(ones(1, nblabels));
weights(1, 21:nblabels) = 0;

I_L = permute(single(im2double(imread('../images/scene1.row3.col2.ppm'))), order_3d);
I_R = permute(single(im2double(imread('../images/scene1.row3.col4.ppm'))), order_3d);
height = size(I_L, 2);
width = size(I_L, 1);

rho = libMEX('computeRho', I_L, I_R, nblabels, lambda);

Phi = single(zeros(height, width, nblabels));
Phi(:, :, 1) = 1;
Phi = permute(Phi, order_3d);
P = single(zeros(height, width, nblabels, 3));
P = permute(P, order_4d);

[Phi, P] = libMEX('iterateUpdate', Phi, P, rho, t1, t2, stepGamma, nbiter);


weights = single(ones(1, nblabels));
weights(1, 21:nblabels) = 0;
img_blurred = libMEX('diffusion', I_L, Phi, weights, bluriter);

imshow(ipermute(img_blurred, order_3d));
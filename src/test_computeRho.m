%%
clc
clear all;

restoredefaultpath;

addpath(genpath('../build/lib/'))

%Params:
lambda = single(50);
t1 = single(1/sqrt(3));
t2 = single(1/sqrt(3));
nbiter=2;
stepGamma = single(1);
nblabels = single(25);
order_3d = [2, 1, 3];
order_4d = [2, 1, 3, 4];
measurement_iter = single(1000);

I_L = permute(single(im2double(imread('../images/scene1.row3.col2.ppm'))), order_3d);
I_R = permute(single(im2double(imread('../images/scene1.row3.col4.ppm'))), order_3d);

height = size(I_L, 1);
width = size(I_L, 2);

cuda_rho = single(computeRho(I_L, I_R, nblabels, 50));
gpu_rho = libMEX('computeRho', I_L, I_R, nblabels, lambda);

diff = abs(cuda_rho - gpu_rho);
diff_min = min(diff(:));
diff_max = max(diff(:));
clear;

restoredefaultpath;
addpath('../build/lib');

% Params:
lambda = single(50);
t1 = single(1/sqrt(3));
t2 = single(1/sqrt(3));
nbiter=1500;
stepGamma = single(1);
nblabels = single(25);
batch_size = single(50);
order_3d = [2, 1, 3];
order_4d = [2, 1, 3, 4];
bluriter = single(100);
weights = single(ones(1, nblabels));
weights(1, 21:nblabels) = 0;

% Read Images
I_L = permute(single(im2double(imread('../images/scene1.row3.col2.ppm'))), order_3d);
I_R = permute(single(im2double(imread('../images/scene1.row3.col4.ppm'))), order_3d);
height = size(I_L, 2);
width = size(I_L, 1);

rho = libMEX('computeRho', I_L, I_R, nblabels, stepGamma, lambda);

Phi = single(zeros(height, width, nblabels));
Phi(:, :, 1) = 1;
Phi = permute(Phi, order_3d); % To make x, y index match with c++ implementation
P = single(zeros(height, width, nblabels, 3));
P = permute(P, order_4d); % To make x, y index match with c++ implementation

labels = single(0:stepGamma:nblabels-1);

f1 = figure('Name','Phi');

for k=1:batch_size:nbiter
    ticiter = tic;

    % Iterarively update phi and p
    [Phi, P] = libMEX('iterateUpdate', Phi, P, rho, t1, t2, stepGamma, batch_size);

    toc(ticiter);

    % Construct Image from Phi and show the image
    % imshow(libMEX('constructImageFromPhi', ipermute(Phi, order_3d), labels),[]);
    imshow(ConstructImageFromPhi(ipermute(Phi, order_3d), labels),[]);
end

f2 = figure('Name','Rho');
[X,Y] = meshgrid(1:size(rho,2),1:size(rho,1));
Z = vecnorm(rho, 2, 3);
surf(X,Y,Z, 'edgecolor', 'none');
colorbar;

% Display the final blurred image
img_blurred = libMEX('diffusion', I_L, Phi, weights, bluriter);
f3 = figure('Name', 'Blurred Image');
imshow(ipermute(img_blurred, order_3d));

disp('End of Program');
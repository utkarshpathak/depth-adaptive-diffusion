function [PKPlus1] = updateP(PhiKPlus1, PK, StepD, rho, stepGamma)
    Sx = size(PK, 1);
    Sy = size(PK, 2);
    Sz = size(PK, 3);
 
    %Y
    ticp = tic;
 	IShift1 = cat(1, PhiKPlus1, zeros(1,Sy,Sz));
 	IShift2 = cat(1, zeros(1,Sy,Sz), PhiKPlus1);
 	IDiff = IShift1 - IShift2;
 	IDiff = IDiff(2:Sx, :, :);
 	IDiff = cat(1, IDiff, zeros(1, Sy, Sz));
 	grad(:,:,:, 2) = IDiff;
    
    %X
 	JShift1 = cat(2, PhiKPlus1, zeros(Sx,1,Sz));
 	JShift2 = cat(2, zeros(Sx,1,Sz), PhiKPlus1);
 	JDiff = JShift1 - JShift2;
 	JDiff = JDiff(:, 2:Sy, :);
 	JDiff = cat(2, JDiff, zeros(Sx, 1, Sz));
 	grad(:,:,:, 1) = JDiff;
 
 	KShift1 = cat(3, PhiKPlus1, zeros(Sx,Sy,1));
 	KShift2 = cat(3, zeros(Sx,Sy,1), PhiKPlus1);
 	KDiff = KShift1 - KShift2;
 	KDiff = KDiff(:, :, 2:Sz);
 	KDiff = cat(3, KDiff, zeros(Sx, Sy, 1));
 	grad(:,:,:, 3) = KDiff/ stepGamma;
 
    PKPlus1 = PK + StepD * grad; %(PhiKPlus1);
  
    %Project on C.
    %Denom = max(1, sqrt(PKPlus1(:,:,:,1) .^ 2 + PKPlus1(:,:,:,2) .^ 2));
    PKPlus1(:,:,:,1) = PKPlus1(:,:,:,1) ./ max(1, abs(PKPlus1(:,:,:,1)));
    PKPlus1(:,:,:,2) = PKPlus1(:,:,:,2) ./ max(1, abs(PKPlus1(:,:,:,2)));
    PKPlus1(:,:,:,3) = PKPlus1(:,:,:,3) ./ max(1, abs(PKPlus1(:,:,:,3)) ./ rho);
    toc(ticp);

%	Sx = size(PK, 1);
%	Sy = size(PK, 2);
%	Sz = size(PK, 3);

%	PKPlus1 = zeros(size(PK));
%	IDiff_test = zeros(size(PK, 1), size(PK, 2), size(PK, 3));
%	JDiff_test = zeros(size(PK, 1), size(PK, 2), size(PK, 3));
%	KDiff_test = zeros(size(PK, 1), size(PK, 2), size(PK, 3));
%	IDiff_test = 0;
%	JDiff_test = 0;
%	KDiff_test = 0;
%    
%	for i=1:Sx
%		for j=1:Sy
%			for k=1:Sz
%				if (i<Sx) 
%					IDiff_test = PhiKPlus1(i+1,j,k) - PhiKPlus1(i,j,k);
%				end
%				if (j<Sy) 
%					JDiff_test = PhiKPlus1(i,j+1,k) - PhiKPlus1(i,j,k);
%				end
%				if (k<Sz) 
%					KDiff_test = (PhiKPlus1(i,j,k+1) - PhiKPlus1(i,j,k)) / stepGamma;
%				end
%
%				PKPlus1(i,j,k,1) = PK(i,j,k,1) + StepD * IDiff_test;
%				PKPlus1(i,j,k,2) = PK(i,j,k,2) + StepD * JDiff_test;
%				PKPlus1(i,j,k,3) = PK(i,j,k,3) + StepD * KDiff_test;
%
%				PKPlus1(i,j,k,1) = PKPlus1(i,j,k,1) / max(1, abs(PKPlus1(i,j,k,1)));
%				PKPlus1(i,j,k,2) = PKPlus1(i,j,k,2) / max(1, abs(PKPlus1(i,j,k,2)));
%				PKPlus1(i,j,k,3) = PKPlus1(i,j,k,3) / max(1, abs(PKPlus1(i,j,k,3)) / rho(i,j,k));
%            end
%		end
%    end
%   fprintf('IDiff and IDiff_test are equal %d\n', isequal(IDiff, IDiff_test));
%	fprintf('JDiff and JDiff_test are equal %d\n', isequal(JDiff, JDiff_test));
%	fprintf('KDiff and KDiff_test are equal %d\n', isequal(KDiff, KDiff_test));
%    
%    fprintf("\n");
end


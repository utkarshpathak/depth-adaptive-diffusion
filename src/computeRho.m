function [rho] = computeRho(I_L,I_R, nblayers, lambda)
rho = zeros(size(I_L, 1), size(I_L, 2), nblayers);

for i = 1:nblayers
   diff = I_L - I_R;
   rho(:, :, i) = lambda .* sqrt(sum(diff .^ 2, 3));
   
   I_R(:, 2:end, :) = I_R(:, 1:end-1, :);
end

end


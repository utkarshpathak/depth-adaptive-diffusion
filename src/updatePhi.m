function [PhiKPlus1] = updatePhi(PhiK, PK, StepP, stepGamma)
    Sx = size(PK, 1);
    Sy = size(PK, 2);
    Sz = size(PK, 3);

    %Y
    ticphi = tic;
	IShift1 = cat(1, PK(1:Sx - 1, :, :, 2), zeros(2,Sy,Sz));
	IShift2 = cat(1, zeros(1,Sy,Sz), PK(:,:,:,2));
	IDiff = IShift1 - IShift2;
	IDiff = IDiff(1:Sx,:,:);

    %X
	JShift1 = cat(2, PK(:, 1:Sy - 1, :,1), zeros(Sx, 2, Sz));
	JShift2 = cat(2, zeros(Sx, 1, Sz), PK(:,:,:,1));
	JDiff = JShift1 - JShift2;
	JDiff = JDiff(:, 1:Sy, :);

	ZShift1 = cat(3, PK(:, :, 1:Sz - 1,3), zeros(Sx, Sy, 2));
	ZShift2 = cat(3, zeros(Sx, Sy, 1), PK(:,:,:,3));
	ZDiff = ZShift1 - ZShift2;
	%Warning! Assume that all quantification steps are equally distributed.
	ZDiff = ZDiff(:, :, 1:Sz) / stepGamma;

	div = IDiff + JDiff + ZDiff;
   	PhiKPlus1 = PhiK + StepP * div;	%(PK);

    %Truncate so it stays in D.
    indexes = PhiKPlus1(:,:,:) < 0;
    PhiKPlus1(indexes == true) = 0;
    indexes = PhiKPlus1(:,:,:) > 1;
    PhiKPlus1(indexes == true) = 1;
    %PhiKPlus1 = (PhiKPlus1>0).*( (PhiKPlus1<1).*PhiKPlus1+(PhiKPlus1>=1) );
    PhiKPlus1(:,:,1) = 1;
    PhiKPlus1(:,:,end) = 0;
    toc(ticphi);

end


%%
clc
clear all;

restoredefaultpath;

addpath(genpath('../build/lib/'))

%Params:
t1 = single(1/sqrt(3));
t2 = single(1/sqrt(3));
nbiter=50;
stepGamma = single(1);
nblabels = single(25);
order_3d = [2, 1, 3];
order_4d = [2, 1, 3, 4];
measurement_iter = single(1000);

I_L = im2double(imread('../images/scene1.row3.col2.ppm'));
I_R = im2double(imread('../images/scene1.row3.col4.ppm'));

height = size(I_L, 1);
width = size(I_L, 2);

rho = single(computeRho(I_L, I_R, nblabels, 50));


Phi = single(zeros(height, width, nblabels));
Phi(:, :, 1) = 1;
P = single(zeros(height, width, nblabels, 3));

for k=1:nbiter
    Phi = updatePhi(Phi, P, t1, stepGamma);
    P = updateP(Phi, P, t2, rho, stepGamma);
end

P_exp = updateP(Phi, P, t2, rho, stepGamma);

P_cuda = libMEX('updateP', permute(Phi, order_3d), permute(P, order_4d), t2, permute(rho, order_3d), stepGamma, measurement_iter);
P_cuda = ipermute(P_cuda, order_4d);

diff = abs(P_exp - P_cuda);
diff_min = min(diff(:));
diff_max = max(diff(:));
#ifndef _MAIN_MEX_CPP_
#define _MAIN_MEX_CPP_

#ifdef USE_MEX

/* system header */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <exception>
#include <functional>
#include <map>
#include <cmath>
#include <memory>
#include <sstream>
#include <vector>
#include <iostream>

/* MEX header */
#include <mex.h> 
#include "matrix.h"

/* fixing error : identifier "IUnknown" is undefined" */
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#endif

#if defined(_WIN32) || defined(_WIN64)
#include <windows.h>
#endif

#include "../lib/cuda/cuda_utils.cuh"
#include "../lib/cuda/update_phi.cuh"
#include "../lib/cuda/update_p.cuh"
#include "../lib/cuda/iterate_update.cuh"
#include "../lib/cuda/compute_rho.cuh"
#include "../lib/cuda/diffusion.cuh"
#include "../lib/cuda/construct_image_from_phi.cuh"

#define MEX_ARGS int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[]

std::vector<mwSize> getDimensions(const mxArray *array) {
    auto num_dims = mxGetNumberOfDimensions(array);
    auto *dims = mxGetDimensions(array);

    return std::vector<mwSize>(dims, dims + num_dims);
}

View1D<float> get_matrix1d(const mxArray *arg, const std::string &var_name) {
    auto dims = getDimensions(arg);
    if (dims.size() != 2) {
        throw std::invalid_argument(var_name + " has wrong number of dimensions: " + std::to_string(dims.size()));
    }
    if (dims[0] != 1) {
        throw std::invalid_argument(var_name + " must be of form (1 x size).");
    }
    if (!mxIsSingle(arg)) {
        throw std::invalid_argument(var_name + " must be type single");
    }

    return {mxGetSingles(arg), make_uint1(dims[1])};
}

View3D<float> get_matrix3d(const mxArray *arg, const std::string &var_name) {
    auto dims = getDimensions(arg);
    if (dims.size() != 3) {
        throw std::invalid_argument(var_name + " has wrong number of dimensions: " + std::to_string(dims.size()));
    }
    if (!mxIsSingle(arg)) {
        throw std::invalid_argument(var_name + " must be type single");
    }

    return {mxGetSingles(arg), make_uint3(dims[0], dims[1], dims[2])};
}

float get_scalar(const mxArray *arg, const std::string &var_name) {
    if (!mxIsSingle(arg) && mxGetNumberOfElements(arg) == 1) {
        throw std::invalid_argument(var_name + " must be scalar of type single");
    }

    return static_cast<float>(mxGetScalar(arg));
}

View4D<float> get_p(const mxArray *arg) {
    auto dims = getDimensions(arg);

    if (dims.size() != 4) {
        throw std::invalid_argument("p_k has wrong number of dimensios: " + std::to_string(dims.size()));
    }
    if (dims[3] != 3) {
        throw std::invalid_argument("the size of the fourth dimension of p_k has to be 3: " + std::to_string(dims[3]));
    }
    if (!mxIsSingle(arg)) {
        throw std::invalid_argument("p_k must be type single");
    }

    return {mxGetSingles(arg), make_uint4(dims[0], dims[1], dims[2], 3)};
}

bool operator == (const uint3 &dim_3d, const uint4 &dim_4d) {
    return dim_3d.x == dim_4d.x && dim_3d.y == dim_4d.y && dim_3d.z == dim_4d.z;
}

bool operator != (const uint3 &dim_3d, const uint4 &dim_4d) {
    return ! (dim_3d == dim_4d);
}

View2D<float> init_matrix(mxArray * &output, const uint2 &dims) {
    std::vector<mwSize> dim_v = {dims.x, dims.y};
    output = mxCreateNumericArray(dim_v.size(), dim_v.data(), mxSINGLE_CLASS, mxREAL);

    return {mxGetSingles(output), dims};
}

View3D<float> init_matrix(mxArray * &output, const uint3 &dims) {
    std::vector<mwSize> dim_v = {dims.x, dims.y, dims.z};
    output = mxCreateNumericArray(dim_v.size(), dim_v.data(), mxSINGLE_CLASS, mxREAL);

    return {mxGetSingles(output), dims};
}

View4D<float> init_matrix(mxArray *&output, const uint4 &dims) {
    std::vector<mwSize> dim_v = {dims.x, dims.y, dims.z, dims.w};
    output = mxCreateNumericArray(dim_v.size(), dim_v.data(), mxSINGLE_CLASS, mxREAL);

    return {mxGetSingles(output), dims};
}

void mex_compute_rho(MEX_ARGS) {

    // argument check 
    if ( nrhs != 5 || nlhs != 1) {
    throw std::invalid_argument("usage: rho = computeRho(I_L, I_R, nblabels, gamma, lambda)\n");
    }

    auto Il = get_matrix3d(prhs[0], "Il");
    auto Ir = get_matrix3d(prhs[1], "Ir");
    auto h = Il.dims().y;
    auto w = Il.dims().x;
    auto nblayers = static_cast<int>(get_scalar(prhs[2], "nblayers"));
    auto gamma = get_scalar(prhs[3], "gamma");
    auto lambda = get_scalar(prhs[4], "lambda");

    if ((Il.dims().y != Ir.dims().y) || (Il.dims().x != Ir.dims().x)) {
    	throw std::invalid_argument("first three dimensions of Il and Ir must agree");
    }
    
    auto rho = init_matrix(plhs[0], make_uint3(w, h, nblayers));
    
    compute_rho(Il, Ir, w, h, nblayers, gamma, lambda, rho);
}

void mex_update_phi(MEX_ARGS) {
    if (nrhs != 5 || nlhs != 1) {
        throw std::invalid_argument("usage: phi_kp1 = updatePhi(phi_k, p_k, tau_p, step_gamma, num_iter)");
    }    

    auto phi_k = get_matrix3d(prhs[0], "phi_k");
    auto p_k = get_p(prhs[1]);
    auto tau_p = get_scalar(prhs[2], "tau_p");
    auto step_gamma = get_scalar(prhs[3], "step_gamma");
    auto num_iter = static_cast<uint>(get_scalar(prhs[4], "num_iter"));

    if (phi_k.dims() != p_k.dims()) {
        throw std::invalid_argument("first three dimensions of phi and p must agree");
    }

    auto phi_kp1 = init_matrix(plhs[0], phi_k.dims());

    auto avg_time = test_update_phi(phi_k, p_k, tau_p, step_gamma, phi_kp1, num_iter);
    mexPrintf("updatePhi avg time: %f\n", avg_time);
}

void mex_update_p(MEX_ARGS) {
    if (nrhs != 6 || nlhs != 1) {
        throw std::invalid_argument("usage: p_kp1 = updateP(phi_k, p_k, tau_d, rho, step_gamma, num_iter)");
    }

    auto phi_k = get_matrix3d(prhs[0], "phi_k");
    auto p_k = get_p(prhs[1]);
    auto tau_d = get_scalar(prhs[2], "tau_d");
    auto rho = get_matrix3d(prhs[3], "rho");
    auto step_gamma = get_scalar(prhs[4], "step_gamma");
    auto num_iter = static_cast<uint>(get_scalar(prhs[5], "num_iter"));


    if (phi_k.dims() != p_k.dims()) {
        throw std::invalid_argument("first three dimensions of phi_k and p_k must agree");
    }

    if (phi_k.size() != rho.size()) {
        throw std::invalid_argument("first three dimensions of phi_k and rho must agree");
    }

    auto p_kp1 = init_matrix(plhs[0], p_k.dims());

    auto avg_time = test_update_p(phi_k, p_k, tau_d, rho, step_gamma, p_kp1, num_iter);
    mexPrintf("updateP avg time: %f\n", avg_time);
}

void mex_iterate_update(MEX_ARGS) {
    if (nlhs != 2 && nrhs != 7) {
        throw std::invalid_argument("usage phi_kpiter, p_kpiter = iterateUpdate(phi_k, p_k, rho, tau_p, tau_d, step_gamma, num_iter)");
    }

    auto phi_k = get_matrix3d(prhs[0], "phi_k");
    auto p_k = get_p(prhs[1]);
    auto rho = get_matrix3d(prhs[2], "rho");
    auto tau_p = get_scalar(prhs[3], "tau_p");
    auto tau_d = get_scalar(prhs[4], "tau_d");
    auto step_gamma = get_scalar(prhs[5], "step_gamma");
    auto num_iter = static_cast<int>(std::ceil(get_scalar(prhs[6], "num_iter")));

    if (phi_k.dims() != p_k.dims()) {
        throw std::invalid_argument("first three dimensions of phi_k and p_k must agree");
    }

    if (phi_k.size() != rho.size()) {
        throw std::invalid_argument("first three dimensions of phi_k and rho must agree");
    }

    if (num_iter < 0) {
        throw std::invalid_argument("number of iterations has to be positive.");
    }

    auto phi_kpiter = init_matrix(plhs[0], phi_k.dims());
    auto p_kpiter = init_matrix(plhs[1], p_k.dims());

    iterate_update(phi_k, p_k, rho, tau_p, tau_d, step_gamma, phi_kpiter, p_kpiter, num_iter, num_iter, 0.f);
}

void mex_construct_image_from_phi(MEX_ARGS) {
    if (nlhs != 1 && nrhs != 5) {
        throw std::invalid_argument("usage image = construct_image_from_phi(phi, image, gamma_min, gamma_max, step_gamma)");
    }

    auto phi = get_matrix3d(prhs[0], "phi");
    auto labels = get_matrix1d(prhs[1], "LabeLQuantification");
    auto image = init_matrix(plhs[0], make_uint2(phi.dims().x, phi.dims().y));

    // construct_image_from_phi(phi, image, labels);
}

void mex_diffusion(MEX_ARGS) {
    if (nrhs != 4 || nlhs != 1) {
        throw std::invalid_argument("usage: result = diffusion(img, phi, weights, num_iter)");
    }

    auto img = get_matrix3d(prhs[0], "img");
    auto phi = get_matrix3d(prhs[1], "phi");
    auto weights = get_matrix1d(prhs[2], "weights");
    auto num_iter = static_cast<uint>(get_scalar(prhs[3], "num_iter"));

    if (img.dims().z != 3) {
        throw std::invalid_argument("Third dimension of img must be 3.");
    }

    if (weights.dims().x != phi.dims().z) {
        throw std::invalid_argument("Dimensions of weights and thrid dimension of phi must agree.");
    }

    if (img.dims().x != phi.dims().x || img.dims().y != phi.dims().y) {
        throw std::invalid_argument("first two dimensions of img and phi must agree.");
    }

    auto result = init_matrix(plhs[0], img.dims());

    test_diffusion(img, phi, weights, num_iter, result);
}

static std::map<std::string, std::function<void(MEX_ARGS)>> cmd_reg {
    {"updatePhi", mex_update_phi},
    {"updateP", mex_update_p},
    {"iterateUpdate", mex_iterate_update},
    {"computeRho", mex_compute_rho},
    {"constructImageFromPhi", mex_construct_image_from_phi},
    {"diffusion", mex_diffusion}
};
            
/* MEX entry function */
void mexFunction(int nlhs, mxArray *plhs[],int nrhs, const mxArray *prhs[]){
    if (nrhs < 1) {
        mexErrMsgTxt("Usage: libMEX(command, args...)");
        return;
    }

    auto cmd = std::string(mxArrayToString(prhs[0]));
    
    if (cmd_reg.count(cmd) == 0) {
        std::stringstream ss;
        ss << "Unknown command '" << cmd << "'. List of supported commands:";
        for (const auto &elem : cmd_reg) {
            ss << "\n" << elem.first;
        }
        
        mexErrMsgTxt(ss.str().c_str());
        return;
    }
    
    try {
        cmd_reg[cmd](nlhs,plhs, nrhs - 1, prhs + 1);
    }
    catch(std::exception &exc) {
        std::stringstream ss;
        ss << cmd << ": " << exc.what();
        mexPrintf("%s", ss.str().c_str());
        mexErrMsgTxt("Error!");
    }
    catch (...) {
        std::stringstream ss;
        ss << cmd << ": Unknown error while executing.";
        mexErrMsgTxt(ss.str().c_str());
    }
}
#endif//USE_MEX

#endif

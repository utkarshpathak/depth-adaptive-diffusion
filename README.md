# Depth Adaptive Diffusion

A matlab code that uses mex functions to use Cuda for image manipulation (depth adaptive diffusion) as per 'A Convex Formulation of Continuous Multi-label Problems', Pock et al. For any image, depth map is computed and background is blurred.

(Matlab wrapper with animation of the optimization can be found in Run.m)

Build instructions
==================

1. Create a build directory in the project root folder

2. Run cmake inside the build directory.

3. Set $MATLAB_ROOT environment variable according to error message.

4. Run make.


Command line interface of bin/run
=================================

-i : left input image

-j : right input image

-n : max. number of iterations (termination criterion)

-p : energy change per pixel threshold to stop iterations (termination criterion)

-e : number of iterations after which energy calculation is done

-l : lambda

-b : number of gamma (layers)

-g : delta gamma

-m : number of iterations for diffusion

-t : foreground disparity threshold (higher disparities will not get blurred)


Commands (results are sensitive to gamma)
=========================================

./bin/run -i ../images/scene1.row3.col2.ppm -j ../images/scene1.row3.col4.ppm

./bin/run -i ../images/scene1.row3.col3.ppm -j ../images/scene1.row3.col4.ppm -n 10000 -b 60 -g 0.5

./bin/run -i ../images/Recycle0.jpg -j ../images/Recycle1.jpg -n 10000 -t 15 -b 50

./bin/run -i ../images/Sword0.jpg -j ../images/Sword1.jpg -n 10000 -t 20 -b 100 -g 0.5

./bin/run -i ../images/Motorcycle0.jpg -j ../images/Motorcycle1.jpg -n 10000 -t 20 -b 50

./bin/run -i ../images/Lab0.jpg -j ../images/Lab1.jpg -n 10000 -t 15 -b 25

./bin/run -i ../images/Umbrella0.jpg -j ../images/Umbrella1.jpg -n 10000

./bin/run -i ../images/bed_room0.jpg -j ../images/bed_room1.jpg -n 10000 -t 33 -b 50
